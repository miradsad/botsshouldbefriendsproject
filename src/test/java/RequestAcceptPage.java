import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.sleep;

public class RequestAcceptPage {
    private final SelenideElement addHim = $x("//span[@class=\"button-pro __small js-add-friend\"]");
    public RequestAcceptPage clickTheAddButton(){
        addHim.click();
        sleep(1000);
        return this;
    }
}
