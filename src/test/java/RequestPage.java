import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.sleep;

public class RequestPage {
    private final SelenideElement request = $x("//*[@id=\"tabpanel-users\"]/div[2]/div/div/div/div[1]/div[2]/div[2]/button/span/span[1]");
    public RequestPage requestFriendship(){
        request.click();
        sleep(1000);
        return this;
    }
}
