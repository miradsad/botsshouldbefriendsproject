import com.codeborne.selenide.WebDriverRunner;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.TestMethodOrder;
import org.testng.annotations.Test;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class MyTest extends BaseTest {
    private final static String MY_URL = "https://ok.ru/";
    private final static String firstBotId = "587712224214";
    private final static String secondBotId = "588042393746";
    private static boolean testFriendRequestExecuted = false;
    @Test
    @Order(1)
    public void testFriendRequest(){
        System.out.println("Первый бот должен сделать реквест");
        AuthPage authPage = new AuthPage(MY_URL);
        authPage.clickGoogleAuthButton();
        String originalWindowHandle = WebDriverRunner.getWebDriver().getWindowHandle();
        System.out.println("Задали оригинальную вкладку");
        for (String windowHandle : WebDriverRunner.getWebDriver().getWindowHandles()) {
            WebDriverRunner.getWebDriver().switchTo().window(windowHandle);
        }
        System.out.println("Переключились на вкладку, которая с авторизацией Google");
        GoogleAuthPage googleAuthPage = new GoogleAuthPage();
        googleAuthPage.inputTheEmail(botFirstLogin).inputThePassword(botFirstPassword);
        WebDriverRunner.getWebDriver().switchTo().window(originalWindowHandle);
        System.out.println("Вернулись к оригинальной вкладке");
        GlobalOkSearch globalOkSearch = new GlobalOkSearch();
        globalOkSearch.searchGloballyById(secondBotId);
        RequestPage requestPage = new RequestPage();
        requestPage.requestFriendship();
        testFriendRequestExecuted = true;
        tearDown();
    }
    @org.junit.jupiter.api.Test
    @Order(2)
    public void addByRequest(){
        if (!testFriendRequestExecuted) {
            testFriendRequest();
        }
        System.out.println("Второй бот должен принять заявку");
        AuthPage authPage2 = new AuthPage(MY_URL);
        authPage2.clickGoogleAuthButton();
        String originalWindowHandle2 = WebDriverRunner.getWebDriver().getWindowHandle();
        System.out.println("Задали оригинальную вкладку");
        for (String windowHandle : WebDriverRunner.getWebDriver().getWindowHandles()) {
            WebDriverRunner.getWebDriver().switchTo().window(windowHandle);
        }
        System.out.println("Переключились на вкладку, которая с авторизацией Google");
        GoogleAuthPage googleAuthPage2 = new GoogleAuthPage();
        googleAuthPage2.inputTheEmail(botSecondLogin).inputThePassword(botSecondPassword);
        WebDriverRunner.getWebDriver().switchTo().window(originalWindowHandle2);
        System.out.println("Вернулись к оригинальной вкладке");
        RequestAcceptPage requestAcceptPage = new RequestAcceptPage();
        requestAcceptPage.clickTheAddButton();
        UserFriends userFriends = new UserFriends();
        Assertions.assertEquals(userFriends.jumpToFriends().hasBotFriend(),firstBotId);
    }
}
