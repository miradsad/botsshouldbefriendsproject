import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;

public class UserFriends {
    private final SelenideElement friends = $x("//*[@id=\"hook_Block_Navigation\"]/div/div/div[3]/a");
    public UserFriends jumpToFriends(){
        friends.click();
        sleep(1000);
        return this;
    }
    public String hasBotFriend(){
        SelenideElement myFriend = $x("//div[@class=\"user-grid-card __s\"]");
        return myFriend.getAttribute("data-id");
    }
}
