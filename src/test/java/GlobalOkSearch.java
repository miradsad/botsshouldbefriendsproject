import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.Keys;

import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.sleep;

public class GlobalOkSearch {
    private final SelenideElement globalSearch = $x("//input[@class=\"input__prt1l __size-m__prt1l input__mofy2 input-field__on39s __right-icon__on39s __white-toolbar__on39s\"]");
    public GlobalOkSearch searchGloballyById(String id){
        globalSearch.setValue(id);
        globalSearch.sendKeys(Keys.ENTER);
        sleep(1000);
        return this;
    }
}
